
const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib')

console.log({
    sum: mylib.sum(1,1),
    random: mylib.random(),
    array: mylib.arrayGen()
})

app.get('/', (req, res) => {
    res.send('Hello World');
})

app.get('/add', (req, res)=>{
    const a = parseInt(req.query.a)
    const b = parseInt(req.query.b)
    res.send(`sum is ${mylib.sum(a,b)}`)
})

app.listen(port, ()=>{
    console.log("Server running in port "+port)
})